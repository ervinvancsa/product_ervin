#include "NonPerishableProduct.h"

NonPerishableProduct::NonPerishableProduct(uint32_t id, std::string name, uint32_t rawPrice, m_type type) : 
	Product(id, name, rawPrice), m_nonPerishableProductType(type)
{
}

m_type NonPerishableProduct::getType()
{
	return m_nonPerishableProductType;
}

uint32_t NonPerishableProduct::getVAT()
{
	return 19;
}

float NonPerishableProduct::getPrice()
{
	return m_rawPrice + m_rawPrice * 0.19;
}

