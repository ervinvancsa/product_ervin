#pragma once
#include <cstdint>
#include <string>
#include <deque>
#include <iostream>

class IPriceable
{
public:
	virtual uint32_t getVAT() = 0;
	virtual float getPrice() = 0;
};

