#pragma once
#include "../Product.h"
#include "NonPerishableProductType.h"
#include <cstdint>

class NonPerishableProduct : public Product
{
private:
	m_type m_nonPerishableProductType;
public:
	NonPerishableProduct(uint32_t id, std::string name, uint32_t rawPrice, m_type type);
	m_type getType();
	uint32_t getVAT() override;
	float getPrice() override;
};

