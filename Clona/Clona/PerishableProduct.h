#pragma once
#include "../Product.h"

class PerishableProduct : public Product
{
private:
	std::string m_expirationDate;
public:
	PerishableProduct(uint32_t id, const std::string name, float rawPrice, const std::string expirationDate);
	std::string getExpirationDate();
	uint32_t getVAT() override;
	float getPrice() override;
};
