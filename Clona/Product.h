#pragma once
#include "Clona/IPriceable.h"

class Product : public IPriceable
{
public:
	Product(uint32_t id, const std::string name, float rawPrice);
	virtual uint32_t getID() = 0;
	virtual std::string getName() = 0;
	virtual float getRawPrice() = 0;
protected:
	uint32_t m_id;
	std::string m_name;
	float m_rawPrice;
};
